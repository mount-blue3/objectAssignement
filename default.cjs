function defaultPropsOfObj(obj,defaults){
    if(typeof obj !== 'object' || obj === 'undefined' || obj === null){
        return [];
    }
    else{
       for(let key in defaults){
        if(!(obj.hasOwnProperty(key))){
            obj[key] = defaults[key];
        }
       } 
    }
    return obj;
}
module.exports = defaultPropsOfObj;