function valuesOfObj(obj){
    if(typeof obj !== 'object' || obj === 'undefined' || obj === null){
        return [];
    }
    else{
        let values = [];
        for(let key in obj){
            if(obj.hasOwnProperty(key)){
                values.push(obj[key]);
            }
        }   
        return values;
    }
}
module.exports = valuesOfObj;