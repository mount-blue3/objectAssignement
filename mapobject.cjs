function mapObjects(obj,cb){
    for(let key in obj){
        obj[key] = cb(obj[key]);
    }
    return obj;
}
module.exports = mapObjects;