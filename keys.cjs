function keysOfObj(obj){
    if(typeof obj !== 'object' || obj === 'undefined' || obj === null){
        return [];
    }
    else{
        let keys = [];
        for(let key in obj){
            if(obj.hasOwnProperty(key)){
                keys.push(key);
            }
        }   
        return keys;
    }
}
module.exports = keysOfObj;