function pairsOfObj(obj){
    if(typeof obj !== 'object' || obj === 'undefined' || obj === null){
        return [];
    }
    else{
        let pairs = [];
        for(let key in obj){
            if(obj.hasOwnProperty(key)){
                let keyValue = [key,obj[key]];
                pairs.push(keyValue);
            }
        }   
        return pairs;
    }
}
module.exports = pairsOfObj;